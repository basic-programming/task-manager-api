const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'santosh.s@simformsolutions.com',
        subject: "Thanks for Joinning in!",
        text: `Welcome to the app, ${name}, \n Let me know how you get along with the app....`,

    })
}

const sendCancelEmail = (email, name) => {
    sgMail.send({
        to: email,
        from: 'santosh.s@simformsolutions.com',
        subject: `Good bye ${name}`,
        text: `Have a great learning ${name}\n Is there any thing we can do to stay connected with you...`
    })
}

module.exports = {
    sendWelcomeEmail,
    sendCancelEmail
}