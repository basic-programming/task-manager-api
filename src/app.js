const log = console.log;
const express = require('express');

require('./db/mongoose');
const userRoute = require('./router/user');
const taskRoute = require('./router/task');

const app = express();

app.use(express.json());
app.use(userRoute);
app.use(taskRoute);

module.exports = app;   