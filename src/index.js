const app = require('./app');
const log = console.log;
const port = process.env.PORT;

app.listen(port, () => {
    log(`Server is up on ${port}...`);
});