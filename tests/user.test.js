const request = require('supertest')
const app = require('../src/app')
const User = require('../src/models/users');
const { userOneId, userOne, setUpDatabase } = require('./fixtures/db')

beforeEach(setUpDatabase)

test('Should sign up a new user', async() => {
    const response = await request(app).post('/users').send({
        name: 'Andrew',
        email: 'andrew@example.com',
        password: 'andrew1234'
    }).expect(201)

    // Assert that the database was changed correctly....
    const user = await User.findById(response.body.user._id)
    expect(user).not.toBeNull()

    // Assertions about the response 
    expect(response.body).toMatchObject({
        user: {
            name: 'Andrew',
            email: 'andrew@example.com'
        },
        token: user.tokens[0].token
    })

    expect(user.password).not.toBe('andrew1234')
});

test('Should login existing user', async() => {
    const response = await request(app).post('/users/login').send({
        email: userOne.email,
        password: userOne.password
    }).expect(200)

    const user = await User.findById(userOneId);
    expect(response.body.token).toBe(user.tokens[1].token)
})

test('Should not login nonexisting user', async() => {
    await request(app).post('/users/login').send({
        email: 'mike@example.com',
        password: 'fjdkfkdnf'
    }).expect(400)
})

test('Should get profile for user', async() => {
    await request(app)
        .get('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test('Should not get profile for unAuthenticated user', async() => {
    await request(app)
        .get('/users/me')
        .send()
        .expect(401)
})

test('Should delete account for authenticated user', async() => {
    await request(app)
        .delete('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)

    // Checking that db has changed correctly.......
    const user = await User.findById(userOneId)
    expect(user).toBeNull()
})

test('Should not delete unAuthorized user', async() => {
    await request(app)
        .delete('/users/me')
        .send()
        .expect(401)
})

test('Sould upload avatar image', async() => {
    await request(app)
        .post('/users/me/avatar')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .attach('avatar', 'tests/fixtures/profile-pic.jpg')
        .expect(200)

    const user = await User.findById(userOneId)
    expect(user.avatar).toEqual(expect.any(Buffer))

    // expect({}).toBe({}) // Error..
    // expect({}).toEqual({}) // worked....
})

test('Should update valid user fields', async() => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            name: 'Jess',
            email: 'jess@email.com',
            password: 'jess1234'
        })
        .expect(200)

    const user = await User.findOne({ email: 'jess@email.com' })
    expect(user).not.toBeNull()
})

test('Should not update invalid field', async() => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            location: "boston"
        })
        .expect(400)
})